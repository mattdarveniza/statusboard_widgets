import json

from django.http import HttpResponse, Http404
from pt import tram


def index(req):
    return HttpResponse('This is the PT index')


def next_tram(req, stop_id):
    try:
        walk_time = req.GET.get('walktime')
        route_no = req.GET.get('route') or '0'
        return HttpResponse(
            json.dumps(tram.next_departure(stop_id, route_no, walk_time)),
            mimetype='application/json')
    except ValueError:
        raise Http404
