import json
import datetime

import requests


def next_departure(stop_id, route_no, walk_time):
    base_url = 'http://tramtracker.com/Controllers/'
    json_headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    response = requests.get(
        base_url + '/GetNextPredictionsForStop.ashx',
        params={
            'stopNo': stop_id,
            'routeNo': route_no,
            'isLowFloor': 'false',
        },
        headers=json_headers).json()
    departures = response['responseObject']

    response = requests.get(
        base_url + '/GetStopInformation.ashx',
        params={'s': stop_id},
        headers=json_headers).json()
    stop_info = response['ResponseObject']

    if not (departures and stop_info):
        raise ValueError

    if (walk_time):
        # Determine if a departure is later than now plus walking time,
        # and if so, ignore current departure and assess the next departure
        # on same criteria.
        for departure in departures:
            departure_time = datetime.datetime.utcfromtimestamp(
                float(departures[0]['PredictedArrivalDateTime'][6:-7]) / 1000)
            if (
                departure_time - datetime.timedelta(minutes=int(walk_time)) >
                datetime.datetime.utcnow()
            ):
                next_stop = departure
                break
        else:
            # If all shown departures are beyond the walking time difference,
            # just show the departure closest to that limit (the latest).
            next_stop = departures[-1]

    else:
        next_stop = departures[0]

    departure = {
        'nextStopTimestamp': next_stop['PredictedArrivalDateTime'][6:-2],
        'routeNo': next_stop['HeadBoardRouteNo'],
        'destination': next_stop['Destination'],
        'stopName': stop_info['StopName'],
        'walkTime': walk_time
    }

    return departure
