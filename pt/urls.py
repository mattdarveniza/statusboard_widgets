from django.conf.urls import patterns, url

from pt import views

urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^next_tram/(?P<stop_id>\d{4})/?$', views.next_tram, name='next_tram'),
)
