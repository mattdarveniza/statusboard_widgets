import datetime

import requests
import dateutil.parser
import pytz

from . import auth_details

baseUrlv2 = "https://bitbucket.org/api/2.0"
baseUrlv1 = "https://bitbucket.org/api/1.0"

username = auth_details.BITBUCKET_USERNAME
password = auth_details.BITBUCKET_PASSWORD
team = auth_details.BITBUCKET_TEAM
auth = (username, password)

# TODO: Place in separate settings file or object, possibly generated from a UI.
history_length = datetime.timedelta(days=7)
tick_length = datetime.timedelta(days=1)
timezone = pytz.timezone('Australia/Melbourne')

min_date = datetime.date.today() - history_length


def datestring_in_range(datestring):
    cDate = dateutil.parser.parse(datestring).astimezone(timezone).date()
    return cDate >= min_date


def get_recent_commits():
    repo_stats = []

    r = requests.get(
        "{base}/user/repositories/".format(base=baseUrlv1),
        auth=auth)

    # Only interested in repos with recent changes
    repos = [repo for repo in r.json() if datestring_in_range(repo['utc_last_updated'])]

    for repo in repos:
        commits = []
        commit_count = 0
        lines_added = 0
        lines_removed = 0

        # Skip personal repos, only interested in team repos
        if repo['owner'] != team:
            continue

        # Get first page of commits
        repo_slug = repo['slug']
        r = requests.get(
            '{base}/repositories/{team}/{repo}/commits'.format(
                base=baseUrlv2, team=team, repo=repo_slug),
            auth=auth)

        # Skip if there are any errors because meh
        if r.status_code != requests.codes.ok:
            print 'Error loading commits for repo {repo}'.format(repo=repo_slug)
            continue

        print 'Loading commits for {repo}'.format(repo=repo_slug)
        resp = r.json()
        c = [
            commit for commit in resp['values']
            if datestring_in_range(commit['date'])]
        commits.extend(c)

        # Skip repos with no recent commits
        if not commits:
            continue

        # Get extra pages on commits
        while len(c) == len(resp['values']) and 'next' in resp:
            print 'Loading more commits for {repo}'.format(repo=repo_slug)
            resp = requests.get(
                '{next}'.format(next=resp['next']),
                auth=auth).json()
            c = [
                commit for commit in resp.json()['values']
                if datestring_in_range(commit['date'])]
            commits.extend(c)

        # Get commit stats
        for commit in commits:
            commit_count += 1

            stats = requests.get(
                '{base}/repositories/{team}/{repo}/changesets/{hash}/diffstat/'
                .format(
                    base=baseUrlv1, team=team, repo=repo_slug,
                    hash=commit['hash']),
                auth=auth).json()

            print 'commit loaded'
            for stat in stats:
                lines_added += stat['diffstat']['added'] or 0
                lines_removed += stat['diffstat']['removed'] or 0

        repo_stats.append({
            'commits': commit_count,
            'lines_added': lines_added,
            'lines_removed': lines_removed,
            'repository': repo_slug,
        })

    return repo_stats
