from django.conf.urls import patterns, url

from codestats import views

urlpatterns = patterns(
    '',
    url(r'^$', views.index, name='index'),
    url(r'^recent_commits/', views.recent_commits, name='recent_commits')
)
