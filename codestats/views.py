import json

from django.http import HttpResponse

from .bitbucket_stats import get_recent_commits


def index(req):
    return HttpResponse('This is the codestats index')


def recent_commits(req):
    return HttpResponse(json.dumps(get_recent_commits()))
