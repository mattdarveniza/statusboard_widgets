/* global utils */

function toggleNowWord(time, id) {
  'use strict';
  if (time < 60 * 1000) {
    document.getElementById(id).setAttribute('hidden', 'true');
  } else {
    document.getElementById(id).removeAttribute('hidden');
  }
}

function updateDepartureInfo(departures) {
  'use strict';
  departures.sort(function (a, b) {
    return a.nextStop - b.nextStop;
  });

  var departure = departures[0];
  var timeToDeparture = departure.nextStop - Date.now();
  var timeToWalk = timeToDeparture - departure.walkTime * 60 * 1000;

  toggleNowWord(timeToDeparture, 'depart-in');
  toggleNowWord(timeToWalk, 'leave-in');

  document.getElementById('route').textContent = departure.routeNo;
  document.getElementById('destination').textContent = departure.destination;
  document.getElementById('stop-name').textContent = departure.stopName;
  document.getElementById('departure-time').textContent =
    utils.epochToDHM(timeToDeparture);
  document.getElementById('leave-time').textContent = utils.epochToDHM(timeToWalk);
}

function showError(message) {
  'use strict';
  var els = document.querySelectorAll('.wrapper > div');
  Array.prototype.forEach.call(els, function(el) {
    el.setAttribute('hidden', 'true');
  });

  var errorEl = document.getElementById('error');
  errorEl.textContent = message;
  errorEl.removeAttribute('hidden');
}

(function() {
  'use strict';
  var departures;
  var stops = [];
  var loadInterval = 10 * 1000;

  function responseError() {
    showError('Service unavailable');
  }

  function nextTramResponse() {
    /* jshint validthis: true */
    if (this.status >= 200 && this.status < 400) {
      var resp = JSON.parse(this.responseText);
      resp.nextStop = new Date(
        parseInt(resp.nextStopTimestamp.split('+')[0], 10));
      departures.push(resp);

      if (departures.length === stops.length) {
        updateDepartureInfo(departures);
      }
    } else {
      responseError();
    }
  }

  function reloadStops() {
    departures = [];
    stops.forEach(function (stop) {
      var r = new XMLHttpRequest();
      var url = '/pt/next_tram/' + stop.stopNo + '?walktime=' + stop.walkTime;
      if (stop.hasOwnProperty('route')) {
        url += '&route=' + stop.route;
      }
      r.open('GET', url);
      r.onload = nextTramResponse;

      r.timeout = loadInterval;
      r.ontimeout = responseError;
      r.onerror = responseError;
      r.send();
    });
  }

  var args = utils.parseUrlArgs();
  // NOTE: Currently statusboard ignores any URL params passed through,
  //       so we have to deal with hardcoded configuration for now.
  if (!args.hasOwnProperty('stops')) {
    // showError('Missing stops in URL' + '\n' + window.location);
    // return;
    stops = [
      {stopNo: '3927', walkTime: 1}, // Molesworth St
      {stopNo: '3177', walkTime: 6, route: '59'}, // Children's Hopsital
    ];
  } else {
    args.stops.split(',').forEach(function (stopNo) {
      stops.push({stopNo: stopNo, walkTime: 0});
    });

    if (args.hasOwnProperty('walktime')) {
      var walkTimes = args.walktime.split(',');
      if (walkTimes.length !== stops.length) {
        showError('Number of stops and number of travel times do not match');
        return;
      }
      walkTimes.forEach(function (time, i) {
        stops[i].walkTime = time;
      });
    }

    if (args.hasOwnProperty('routes')) {
      var routes = args.routes.split(',');
      if (routes.length !== stops.length) {
        showError('Number of routes must equal number of stops');
        return;
      }
      routes.forEach(function (route, i) {
        if (route) {
          stops[i].route = route;
        }
      });
    }
  }

  reloadStops();
  window.setInterval(reloadStops, loadInterval);
})();

