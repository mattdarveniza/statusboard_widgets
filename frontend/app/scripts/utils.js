/* jshint unused: false */
var utils = {
  epochToDHM: function(epoch) {
    'use strict';
    if (epoch < 60 * 1000) {
      return 'NOW';
    }

    var days = Math.floor(epoch / 1000 / 60 / 60 / 24);
    var hours = Math.floor(epoch / 1000 / 60 / 60 - days * 24);
    var minutes = Math.floor(epoch / 1000 / 60 - hours * 60);

    var dhm = '';
    if (days) {
      dhm += days + 'd ';
    }
    if (hours) {
      dhm += hours + 'h ';
    }
    if (minutes) {
      dhm += minutes + 'm';
    }
    return dhm.trim();
  },
  parseUrlArgs: function() {
    'use strict';
    var query = location.search.substr(1);
    var data = query.split('&');
    var args = {};
    data.forEach(function (d) {
      var item = d.split('=');
      args[item[0]] = item[1];
    });
    return args;
  },
};