/* global utils */
(function() {
  'use strict';
  var args = utils.parseUrlArgs();
  if (!args.hasOwnProperty('origin') || !args.hasOwnProperty('destination')) {
    args = _.merge(args, {
      origin: '419 Abbotsford St North Melbourne',
      destination: '180 Albert St Windsor',
    });
  }
  if (!args.hasOwnProperty('mode')) {
    args.mode = 'driving';
  }

  var r = new XMLHttpRequest();
  var url = encodeURI(
    '/maps/api/distancematrix/json' +
    '?origins=' + args.origin +
    '&destinations=' + args.destination +
    '&mode=' + args.mode +
    '&sensor=false');
  r.open('GET', url);
  r.onload = function() {
    var data = JSON.parse(r.response);
    console.log(url);
    console.log(data);
  };
  r.send();
})();