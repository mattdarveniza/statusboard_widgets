from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    '',
    url(r'^pt/', include('pt.urls')),
    url(r'^codestats/', include('codestats.urls'))
)
